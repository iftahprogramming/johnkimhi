﻿using System;
using System.Collections.Generic;
using System.Linq;
using JohnKimhi.BL.GlobalManagers;
using JohnKimhi.Global;
using Newtonsoft.Json;
using JohnKimhi.DAL.IO;
using System.Diagnostics;
using System.Windows.Threading;

namespace JohnKimhi.BL
{
    class Logic
    {
        #region DM
        private ListSensors _Sensors;
        public static bool loaded = false;
        #endregion

        #region Instance
        private static Logic instance;
        public static Logic Instance { get { return instance ?? (instance = new Logic()); } }
        #endregion

        #region Props
        public Rs232Manager Rs232Connection;
        public Stopwatch Counter = new Stopwatch();
        public DispatcherTimer WaitOrGoTimer = new DispatcherTimer();
        public List<DetectionManager> detectionManagers;
        public XlManager xlManager;
        public UIStateManager UIStateManager = new UIStateManager();
        public int DetectionManagersIndex;
        public ListSensors Sensors
        {
            get { return _Sensors; }
            set
            {
                _Sensors = value;
                UIStateManager.SensorsName = _Sensors.SensorsName();
            }
        }
        #endregion

        #region Init
        public void Init()
        {
            FilesClass.CreateDirectory(Environment.CurrentDirectory + @"\Images");
            Rs232Connection = new Rs232Manager(CommandTimeout, AnalyzeData);
            xlManager = new XlManager();
            UIStateManager.TimerCRan = false;

            Counter.Start();

            #region Recording WaitOrGo Timer
            WaitOrGoTimer.Interval = new TimeSpan(0, 0, 0, 0, 500);
            WaitOrGoTimer.Tick += WaitOrGoTimer_Tick;
            WaitOrGoTimer.Start();
            #endregion

            #region Threshould Timer
            UIStateManager.timerC.Interval = new TimeSpan(0, 0, 1);
            UIStateManager.timerC.Tick += UIStateManager.TimerC_Tick;
            #endregion

            #region Recording Manager 
            detectionManagers = new List<DetectionManager>();
            detectionManagers.Add(new DetectionManager());
            detectionManagers.Add(new DetectionManager());
            detectionManagers.Add(new DetectionManager());
            #endregion

            #region Load Json
            UIStateManager.PortsNames = Rs232Connection.PortNames;
            LoadSensors(FilesClass.Read(Consts.SensorsFileDirectory, Consts.FileName));
            #endregion
        }

        #region TimerTick Event
        private void WaitOrGoTimer_Tick(object sender, EventArgs e)
        {
            if (UIStateManager.RecClicked)
            {
                TimeSpan limitTimeSpan = new TimeSpan(0, 0, 0, 5, 200);
                if (Counter.Elapsed < limitTimeSpan)
                    UIStateManager.WaitOrGo = false;
                else
                    UIStateManager.WaitOrGo = true;
            }
        }
        #endregion

        #endregion

        #region Connection Timeout
        private void CommandTimeout()
        {
            Rs232Connection.ClearReceivingList();
        }
        #endregion

        #region Data Analyzer Function
        /// <summary>
        /// Analyze for Recieved Data
        /// </summary>
        /// <param name="received">The Received Packet</param>
        public void AnalyzeData(List<byte> received, byte OperationCode)
        {
            switch (OperationCode)
            {
                //Ok or End
                case Consts.OkOrEndMode:
                    OkayOrEndOperation(received);
                    break;

                //After Boot Time has ended this will be sent
                case Consts.PollingWindowMode:
                    PollingWindowOperation(received);
                    break;

                //After Threshould testing has been checked this will be sent
                case Consts.ThreshouldCountMode:
                    ThresouldCountOperation(received);
                    break;

                //Handles Run or Record Operation
                case Consts.LiveWindowMode:
                    LiveWindowOperation(received);
                    break;

                //Sets Burned Threshould
                case Consts.ThreshouldBurnedMode:
                    ThreshouldBurnedOperation(received);
                    break;
            }
        }
        #endregion

        #region Analyze Data Operation Void Handlers
        #region OkayOrEnd Operation
        /// <summary>
        /// Checks weather end byte or okay byte has been recieved
        /// </summary>
        /// <param name="PacketRecieved"></param>
        private void OkayOrEndOperation(List<byte> PacketRecieved)
        {
            if (PacketRecieved[0] == Consts.EndSample_Replay)
                ResetParams();

            if (UIStateManager.InProgMode && PacketRecieved[0] == Consts.OK_Replay)
                System.Windows.Forms.MessageBox.Show("Okay");

            if (UIStateManager.InProgMode && PacketRecieved[0] == Consts.NotAknowledge_Reply)
                System.Windows.Forms.MessageBox.Show("Packet NOT Acknowledged");
        }
        #endregion

        #region Polling Window Operation
        /// <summary>
        /// Boots system and updated sensors
        /// </summary>
        /// <param name="PacketRecieved"></param>
        private void PollingWindowOperation(List<byte> PacketRecieved)
        {
            UIStateManager.TestingThreshould = true;
            UIStateManager.timerC.Start();
            SensorDetailsUpdate(PacketRecieved[0], PacketRecieved[1]);
            Rs232Connection.ClearReceivingList();
        }
        #endregion

        #region Threshould Count Operation
        /// <summary>
        /// Handles Operation in which timer has ended
        /// </summary>
        /// <param name="PacketRecieved"></param>
        private void ThresouldCountOperation(List<byte> PacketRecieved)
        {
            try
            {
                UIStateManager.TimerCRan = true;
                UIStateManager.TestingThreshould = false;
                UIStateManager.Sensor1ActualThreshold = (PacketRecieved.ConertByteToInt(0) + int.Parse(UIStateManager.Sensor1Threshold)).ToString();
                UIStateManager.Sensor2ActualThreshold = (PacketRecieved.ConertByteToInt(4) + int.Parse(UIStateManager.Sensor2Threshold)).ToString();
                Rs232Connection.ClearReceivingList();
            }
            catch { }
        }
        #endregion

        #region Live Window Operation
        /// <summary>
        /// Handles Run Or Record Mode according to tab selected
        /// </summary>
        /// <param name="PacketRecieved"></param>
        private void LiveWindowOperation(List<byte> PacketRecieved)
        {
            if (UIStateManager.RunOrRecordMode)
                RunModeAnalyzer(PacketRecieved);
            else
            {
                if (UIStateManager.RecClicked)
                {
                    RecordModeAnalyzer(PacketRecieved);
                }
            }
            Rs232Connection.ClearReceivingList();
        }
        #endregion

        #region ThreshouldBurnedOperation
        /// <summary>
        /// Sets both sensor's burned threshould
        /// </summary>
        /// <param name="PacketsReceived"></param>
        private void ThreshouldBurnedOperation(List<byte> PacketsReceived)
        {
            UIStateManager.Sensor1Threshold = (PacketsReceived.ConertByteToInt(0)).ToString();
            UIStateManager.Sensor2Threshold = (PacketsReceived.ConertByteToInt(0)).ToString();
        }
        #endregion 
        #endregion

        #region RunModeAnalyzer
        public void RunModeAnalyzer(List<byte> received)
        {
            UIStateManager.Sens1TrigNum = received.ConertByteToInt((int)Consts.ReadedIndex.Sens1Trigger).ToString();
            UIStateManager.Sens2TrigNum = received.ConertByteToInt((int)Consts.ReadedIndex.Sens2Trigger).ToString();

            Debug.WriteLine("Run1: " + UIStateManager.Sens1TrigNum);
            Debug.WriteLine("Run2: " + UIStateManager.Sens2TrigNum);

            if ((received[(int)Consts.ReadedIndex.LEDs] & (byte)0x01) == 0x01)
                UIStateManager.FirstLEDcolor = true;
            else
                UIStateManager.FirstLEDcolor = false;

            if ((received[(int)Consts.ReadedIndex.LEDs] & (byte)0x02) == 0x02)
                UIStateManager.SecondLEDcolor = true;
            else
                UIStateManager.SecondLEDcolor = false;

            if ((received[(int)Consts.ReadedIndex.LEDs] & (byte)0x04) == 0x04)
            {
                UIStateManager.BothLEDcolor = true;
                Console.Beep();
            }
            else
                UIStateManager.BothLEDcolor = false;
        }
        #endregion

        #region RecordModeAnalyzer
        public void RecordModeAnalyzer(List<byte> received)
        {
            if (PassByBuffer())
            {
                Detection detection = new Detection();

                detection.FirstNumberOfDetection = received.ConertByteToInt((int)Consts.ReadedIndex.Sens1Trigger).ToString();

                detection.SecondNumberOfDetection = received.ConertByteToInt((int)Consts.ReadedIndex.Sens2Trigger).ToString();

                Debug.WriteLine("Record1: " + detection.FirstNumberOfDetection);
                Debug.WriteLine("Record2: " + detection.SecondNumberOfDetection);

                if (detection.FirstNumberOfDetection.Equals("0") || detection.SecondNumberOfDetection.Equals("0"))
                {
                    Rs232Connection.ClearReceivingList();
                    return;
                }

                Console.Beep();
                detectionManagers[DetectionManagersIndex].Detections.Add(detection);
            }
        }
        #endregion

        #region PassByBuffer
        /// <summary>
        /// A meathod that handles recording without useless indications - one detection per passing
        /// </summary>
        /// <returns></returns>
        public bool PassByBuffer()
        {
            Counter.Stop();
            TimeSpan currentTime = Counter.Elapsed;
            //Logic.instance.UIStateManager.PollingWindow
            TimeSpan limitTimeSpan = new TimeSpan(0, 0, 0, 5, 200);
            if (currentTime >= limitTimeSpan)
            {
                Counter.Restart();
                return true;
            }
            else
            {
                Counter.Restart();
                Rs232Connection.ClearReceivingList();
                return false;
            }
        }
        #endregion

        #region Reset LEDs
        public void ResetParams()
        {
            UIStateManager.FirstLEDcolor = false;
            UIStateManager.SecondLEDcolor = false;
            UIStateManager.BothLEDcolor = false;
        }
        #endregion

        #region Update Sensor Details
        /// <summary>
        /// A func that recieves Id's and updates the name and threashould of each sensor
        /// </summary>
        /// <param name="sensor1ID"></param>
        /// <param name="sensor2ID"></param>
        public void SensorDetailsUpdate(byte sensor1ID, byte sensor2ID)
        {
            foreach (var sensor in Sensors.SensorsList)
            {
                if (sensor.ID == sensor1ID)
                {
                    UIStateManager.Sensor1Name = sensor.Name;
                    UIStateManager.Sensor1Threshold = sensor.Threshold.ToString();
                }
                else if (sensor.ID == sensor2ID)
                {
                    UIStateManager.Sensor2Name = sensor.Name;
                    UIStateManager.Sensor2Threshold = sensor.Threshold.ToString();
                }
            }
        }
        #endregion

        #region Load Sensors
        /// <summary>
        /// Loads Sensors Json on program init
        /// </summary>
        /// <param name="json"></param>
        public void LoadSensors(string json)
        {
            try
            {
                Sensors = JsonConvert.DeserializeObject<ListSensors>(json);
            }
            catch
            {
                Sensors = new ListSensors();
            }
        }
        #endregion

        #region Connect
        /// <summary>
        /// A function that opens port and connects to the serial port
        /// </summary>
        /// <param name="portName">Name of port (COM)</param>
        /// <param name="BaudRate">BaudRate of Port</param>
        public void Connect(string portName, int BaudRate)
        {
            UIStateManager.IsConnected = Rs232Connection.OpenPort(portName, BaudRate);
        }
        #endregion

        #region Add Sensor
        /// <summary>
        /// Adds Sensor to Json lib
        /// </summary>
        /// <param name="name">Name of Sensor</param>
        /// <param name="description">Sensor Description</param>
        /// <param name="imageSource">Image of Sensor</param>
        public void AddSensor(string name, string description, string imageSource)
        {
            Sensors.SensorsList.Add(new Sensor()
            {
                Name = name,
                Description = description,
                //ImageName = imageSource,
                ID = Sensors.SensorsList.Count() + Consts.FirstSNSensors
            });
            UIStateManager.SensorsName = _Sensors.SensorsName();
            string json = JsonConvert.SerializeObject(Sensors);

            FilesClass.Save(Consts.SensorsFileDirectory, Consts.FileName, json);
        }
        #endregion

        #region Sensor Update
        /// <summary>
        /// Updates sensors params 
        /// </summary>
        /// <param name="SensorName"></param>
        /// <param name="threshold"></param>
        public void UpdateSensor(string SensorName, int threshold)
        {
            for (int i = 0; i < Sensors.SensorsList.Count; i++)
            {
                if (Sensors.SensorsList[i].Name == SensorName)
                {
                    Sensors.SensorsList[i].Threshold = threshold;
                    break;
                }
            }
            string json = JsonConvert.SerializeObject(Sensors);

            FilesClass.Save(Consts.SensorsFileDirectory, Consts.FileName, json);
        }
        #endregion
    }
}
