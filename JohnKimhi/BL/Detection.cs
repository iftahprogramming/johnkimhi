﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JohnKimhi.BL
{
    public class Detection :ObservableObject
    {
        public bool FirstLED { get; set; }
        public bool SecondLED { get; set; }

        public bool BothLEDs;
        public string FirstNumberOfDetection { get; set; }
        public string SecondNumberOfDetection { get; set; }

        public Detection()
        {
        }
    }
}
