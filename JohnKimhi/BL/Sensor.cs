﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace JohnKimhi.BL
{
    public class Sensor
    {
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("ID")]
        public int ID { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("ImageName")]
        public string ImageName { get; set; }

        [JsonProperty("Threshold")]
        public double Threshold { get; set; }    
    }

    public class ListSensors
    {
        [JsonProperty("Sensors")]
        public List<Sensor> SensorsList { get; set; }

        public List<string> SensorsName()
        {
            
            List<string> NamesList = new List<string>();

            foreach(var sensor in SensorsList ?? (SensorsList=new List<Sensor>()))
            {
                NamesList.Add(sensor.Name);
            }

            return NamesList;
        }
    }


}
