﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace JohnKimhi.BL
{
    public class DetectionManager : ObservableObject
    {
        private int distance;
        private ObservableCollection<Detection> detections;
        private string detectionsNum;

        public int Distance
        {
            get { return distance; }
            set
            {
                distance = value;
                RaisePropertyChangedEvent("Distance");
            }
        }

        public ObservableCollection<Detection> Detections
        {
            get { return detections; }
            set
            {
                detections = value;
                DetectionsNum = detections.Count().ToString();
                RaisePropertyChangedEvent("DetectionsNum");
            }
        }

        public string DetectionsNum
        {
            get { return detections.Count().ToString(); }
            set
            {
                detectionsNum = detections.Count().ToString();
                RaisePropertyChangedEvent("DetectionsNum");
            }
        }

        public int ActualNumberOfPassbys { get; set; }

        public DetectionManager()
        {
            Detections = new ObservableCollection<Detection>();
            distance = 0;
            Detections.CollectionChanged += Detections_CollectionChanged;
            ActualNumberOfPassbys = 0;
        }

        private void Detections_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (Detections.Count == Global.Consts.numberOfPassBys)
            {
                Logic.Instance.Counter.Reset();
                Logic.Instance.UIStateManager.RecClicked = false;

            }
        }
    }
}
