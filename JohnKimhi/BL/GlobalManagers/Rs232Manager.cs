﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilitiesLib.DAL;
using UtilitiesLib.Object;
using JohnKimhi.Global;
using System.Threading;
using System.Diagnostics;

namespace JohnKimhi.BL.GlobalManagers
{
    public class Rs232Manager
    {
        #region DM
        private Rs232Connection connection;
        private List<byte> RecievedData = new List<byte>();
        #endregion

        #region Props
        public string[] PortNames
        {
            get
            {
                return DeviceComm.GetPortNames();
            }
        }
        public delegate void AnalyzeDataDelegate(List<byte> value, byte OperationCode);
        public AnalyzeDataDelegate AnalyzeData;
        public int ReceivingLength { get; set; }
        #endregion

        #region CTOR
        public Rs232Manager(CommandTimeout CommandTimeout, AnalyzeDataDelegate analyzeData)
        {
            connection = new Rs232Connection();
            connection.DataReceivedISR += SampleReceived;
            connection.UseISR = true;
            connection.Timeout += CommandTimeout;
            AnalyzeData = analyzeData;
        }
        #endregion

        #region PrivateMethods
        #region Sample Data Recieved
        /// <summary>
        /// Handles DataRecieved from serialPort
        /// </summary>
        /// <param name="unbufferedData"></param>
        private void SampleReceived(List<byte> unbufferedData)
        {
            try
            {
                ClearReceivingList();
                RecievedData = unbufferedData;

                HandleUnbufferedData(RecievedData);

                byte OperationCode = Consts.BufferedPacketRecieved[0];
                byte LengthOfPacket = Consts.BufferedPacketRecieved[1];
                byte CRC = Consts.BufferedPacketRecieved[LengthOfPacket - 1];

                if (Consts.BufferedPacketRecieved.Count == LengthOfPacket)
                {
                    if (CrcCheck(unbufferedData, CRC))
                    {
                        SendDataToAnalyze(Consts.BufferedPacketRecieved, OperationCode);
                    }
                    else
                        Debug.WriteLine("Error Parsing Packet - CRC shows error");
                }
                else
                    Debug.WriteLine("Error Parsing Packet - Wrong Length");
            }
            catch { Debug.WriteLine("Error Parsing Packet - packetList NULL"); }
        }
        #endregion

        #region CRC Check Func
        /// <summary>
        /// Crc Check to DataRecieved
        /// </summary>
        /// <param name="packetList"></param>
        /// <param name="CRC"></param>
        /// <returns></returns>
        private bool CrcCheck(List<byte> packetList, byte CRC)
        {
            Byte Sum = 0x00;
            for (int i = 0; i < packetList.Count; i++)
                Sum ^= packetList[i];
            return Sum == CRC;
        }
        #endregion

        #region Handles Unbuffered Data
        /// <summary>
        /// Handles length to keep data from being lost
        /// </summary>
        /// <param name="unbufferedData"></param>
        private void HandleUnbufferedData(List<byte> unbufferedData)
        {

            for (int i = 0; i < unbufferedData.Count; i++)
            {
                Consts.BufferedPacketRecieved.Add(unbufferedData[i]);
            }

            byte LengthOfPacket = Consts.BufferedPacketRecieved[1];

            if (unbufferedData.Count >= LengthOfPacket)
            {
                for (int i = LengthOfPacket; i < Consts.BufferedPacketRecieved.Count; i++)
                {
                    Consts.TempPacketRecieved.Clear();
                    Consts.TempPacketRecieved.Add(unbufferedData[i]);
                    Consts.BufferedPacketRecieved.RemoveAt(i);
                }
            }
        }
        #endregion

        #region Sends Data to Analyzer
        /// <summary>
        /// Sends the data to analyzeData via Logic
        /// </summary>
        /// <param name="packetList"></param>
        /// <param name="OperationCode"></param>
        private void SendDataToAnalyze(List<byte> packetList, byte OperationCode)
        {
            packetList.RemoveAt(0);
            packetList.RemoveAt(1);
            packetList.RemoveAt(packetList.Count - 1);

            connection.FullReplyReceived();
            connection.ClearBuffer();
            AnalyzeData(packetList, OperationCode);
            RecievedData.Clear();

            Consts.BufferedPacketRecieved = Consts.TempPacketRecieved;
        } 
        #endregion
        #endregion

        #region Methods

        #region Open Port
        /// <summary>
        /// Opens the SerialPort
        /// </summary>
        public bool OpenPort(string PortName, int BaudRate)
        {
            //Close the current connection
            if (connection.isPortOpen())
            {
                connection.ClosePort();
            }

            string[] portNames = DeviceComm.GetPortNames();
            //Set the port
            connection.SetPortName(PortName);
            connection.SetBaudRate(BaudRate);
            try
            {
                //Connect
                connection.OpenPort();
                return true;
            }
            catch
            {
                //Connection failed
                return false;
            }
        }
        #endregion

        #region ClearData
        /// <summary>
        /// Clears the receiver's list of bytes
        /// </summary>
        public void ClearReceivingList()
        {
            RecievedData.Clear();
        }
        #endregion

        #region Send Command
        /// <summary>
        /// Sending command in serial port
        /// </summary>
        /// <param name="cmd">
        /// cmd is a command from 
        /// in UtilitiesLib
        /// </param>
        public void SendCommand(CommCommand cmd)
        {

            ClearReceivingList();
            Close();
            connection.OpenPort();
            connection.ClearBuffer();
            ReceivingLength = cmd.ReplyLength;
            connection.SendCommand(cmd);

        }
        #endregion

        public void SendCommandWithDelay(CommCommand cmd)
        {
            foreach (byte b in cmd.Command)
            {
                List<byte> list = new List<byte>();
                list.Add(b);
                CommCommand command = new CommCommand("OneBye", 0, 3000, 0, list, null);
                ClearReceivingList();
                //Close();
                //connection.OpenPort();
                connection.ClearBuffer();
                connection.SendCommand(command);
                Thread.Sleep(10);
            }
            ReceivingLength = cmd.ReplyLength;

        }

        /// <summary>
        /// Close serialPort
        /// </summary>
        public bool Close()
        {
            try
            {
                if (connection.isPortOpen())
                {
                    connection.ClosePort();
                    return true;
                }
            }
            catch { }
            return false;
        }
        #endregion
    }
}
