﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace JohnKimhi.BL.GlobalManagers
{
    class UIStateManager : ObservableObject
    {

        #region RS232Binding
        #region DM
        private string[] _portsNames;
        private bool _isConnected;
        private bool _isNotConnected;
        private string _portName;
        private string _baudRate;
        #endregion

        #region Prop
        public string[] PortsNames
        {
            get { return _portsNames; }
            set
            {
                _portsNames = value;
                RaisePropertyChangedEvent("PortsNames");
            }
        }

        public string PortName
        {
            get { return _portName; }
            set
            {
                _portName = value;
                RaisePropertyChangedEvent("PortName");
            }
        }

        public string BaudRate
        {
            get { return _baudRate; }
            set
            {
                _baudRate = value;
                RaisePropertyChangedEvent("BaudRate");
            }
        }

        public bool IsConnected
        {
            get { return _isConnected; }
            set
            {
                _isConnected = value;
                IsNotConnected = !value;
                RaisePropertyChangedEvent("IsConnected");
            }
        }

        public bool IsNotConnected
        {
            get { return _isNotConnected; }
            set
            {
                _isNotConnected = value;
                RaisePropertyChangedEvent("IsNotConnected");
            }
        }


        #endregion
        #endregion

        /// <summary>
        /// States Weather Progress Mode is selected
        /// </summary>
        private bool inProgMode;
        public bool InProgMode
        {
            get { return inProgMode; }
            set
            {
                inProgMode = value;
                RaisePropertyChangedEvent("InProgMode");
            }
        }

        /// <summary>
        /// Is the program Mode Password correct
        /// </summary>
        private bool _isProgModePassCorrect;
        public bool IsProgModePassCorrect
        {
            get { return _isProgModePassCorrect; }
            set
            {
                _isProgModePassCorrect = value;
                RaisePropertyChangedEvent("IsProgModePassCorrect");
            }
        }

        /// <summary>
        /// Polling Window - Should be used for "pass by buffer" function. Currently not used - חלון דגימה
        /// </summary>
        private double pollingWindow;
        public double PollingWindow
        {
            get { return pollingWindow; }
            set
            {
                pollingWindow = value;
                RaisePropertyChangedEvent("PollingWindow");
            }
        }

        /// <summary>
        /// List that contains the name of the sensors
        /// </summary>
        private List<string> _SensorsName;

        public List<string> SensorsName
        {
            get { return _SensorsName; }
            set
            {
                _SensorsName = value;
                RaisePropertyChangedEvent("SensorsName");
            }
        }

        #region Sensors Detail
        #region Sensor Name
        private string sensor1Name;
        public string Sensor1Name
        {
            get { return sensor1Name; }
            set
            {
                //if (value.Equals("") || value.Equals(null))
                //    sensor1Name ="חיישן 1";

                sensor1Name = value;
                RaisePropertyChangedEvent("Sensor1Name");


            }
        }
        private string sensor2Name;
        public string Sensor2Name
        {
            get { return sensor2Name; }
            set
            {
                //if (value.Equals("") || value.Equals(null))
                //    sensor1Name="חיישן 2";

                sensor2Name = value;
                RaisePropertyChangedEvent("Sensor2Name");
            }
        }
        #endregion

        #region Sensor Threshold
        //Burned
        private string sensor1Threshold;
        public string Sensor1Threshold
        {
            get { return sensor1Threshold; }
            set
            {
                sensor1Threshold = value;
                RaisePropertyChangedEvent("Sensor1Threshold");
            }
        }

        private string sensor2Threshold;
        public string Sensor2Threshold
        {
            get { return sensor2Threshold; }
            set
            {
                sensor2Threshold = value;
                RaisePropertyChangedEvent("Sensor2Threshold");
            }
        }

        //Actual
        private string sensor1ActualThreshold;
        public string Sensor1ActualThreshold
        {
            get { return sensor1ActualThreshold; }
            set
            {
                sensor1ActualThreshold = value;
                RaisePropertyChangedEvent("Sensor1ActualThreshold");
            }
        }

        private string sensor2ActualThreshold;
        public string Sensor2ActualThreshold
        {
            get { return sensor2ActualThreshold; }
            set
            {
                sensor2ActualThreshold = value;
                RaisePropertyChangedEvent("Sensor2ActualThreshold");
            }
        }
        #endregion

        #region Sensors Trigger Number
        private string sens1TrigNum;
        public string Sens1TrigNum
        {
            get { return sens1TrigNum; }
            set
            {
                sens1TrigNum = value;
                RaisePropertyChangedEvent("Sens1TrigNum");
            }
        }

        private string sens2TrigNum;
        public string Sens2TrigNum
        {
            get { return sens2TrigNum; }
            set
            {
                sens2TrigNum = value;
                RaisePropertyChangedEvent("Sens2TrigNum");
            }
        }
        #endregion
        #endregion


        #region LED Indications
        private bool firstLEDcolor;
        public bool FirstLEDcolor
        {
            get { return firstLEDcolor; }
            set
            {
                firstLEDcolor = value;
                RaisePropertyChangedEvent("FirstLEDcolor");
            }
        }
        private bool secondLEDcolor;
        public bool SecondLEDcolor
        {
            get { return secondLEDcolor; }
            set
            {
                secondLEDcolor = value;
                RaisePropertyChangedEvent("SecondLEDcolor");
            }
        }
        private bool bothLEDcolor;
        public bool BothLEDcolor
        {
            get { return bothLEDcolor; }
            set
            {
                bothLEDcolor = value;
                RaisePropertyChangedEvent("BothLEDcolor");
            }
        }
        #endregion

        /// <summary>
        /// True - Run, False - Record
        /// </summary>
        private bool runOrRecordMode;
        public bool RunOrRecordMode
        {
            get { return runOrRecordMode; }
            set
            {
                runOrRecordMode = value;
                RaisePropertyChangedEvent("RunOrRecordMode");
            }
        }

        /// <summary>
        /// States Weather a Range testing has started - רצפת רעש
        /// </summary>
        private bool testingThreshould = false;
        public bool TestingThreshould
        {
            get { return testingThreshould; }
            set
            {
                testingThreshould = value;
                TestingThreshouldOposite = value;
                RaisePropertyChangedEvent("TestingThreshould");
            }
        }

        /// <summary>
        /// An opposite value of TestingRange - for binding purposes
        /// </summary>
        public bool TestingThreshouldOposite
        {
            get { return !testingThreshould; }
            set
            {
                testingThreshould = value;
                RaisePropertyChangedEvent("TestingThreshouldOposite");
            }
        }

        /// <summary>
        /// States Weather a Recording has started
        /// </summary>
        private bool recClicked;
        public bool RecClicked
        {
            get { return recClicked; }
            set
            {
                recClicked = value;
                isRecording = value;
                RaisePropertyChangedEvent("RecClicked");
            }
        }

        private bool isRecording;
        public bool IsRecording
        {
            get { return recClicked; }
            set
            {
                isRecording = value;
                RaisePropertyChangedEvent("IsRecording");
            }
        }

        /// <summary>
        /// States Weather a data is being analyzed
        /// </summary>
        private bool waitOrGo;
        public bool WaitOrGo
        {
            get { return waitOrGo; }
            set
            {
                waitOrGo = value;
                RaisePropertyChangedEvent("WaitOrGo");
            }
        }

        /// <summary>
        /// Ticks to display based on timer
        /// </summary>
        public int tick = Global.Consts.Ticks;
        public int Tick
        {
            get { return tick; }
            set
            {
                tick = value;
                RaisePropertyChangedEvent("Tick");
            }
        }

        /// <summary>
        /// Timer for Range Testing
        /// </summary>
        public DispatcherTimer timerC = new DispatcherTimer();
        public DispatcherTimer Timer
        {
            get { return timerC; }
            set
            {
                timerC = value;
                RaisePropertyChangedEvent("Timer");
                //TimerC = value.Interval.ToString();
            }
        }

        /// <summary>
        /// Timer Tick event to be used dinamicaly
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void TimerC_Tick(object sender, EventArgs e)
        {
            if (Tick > 0)
                Tick--;
            else
            {
                timerC.Stop();
                Tick = Global.Consts.Ticks;
            }
        }

        /// <summary>
        /// States weather the timer has already ran - dataRecieved analyzeData willl change accordingly
        /// </summary>
        private bool timerCRan;
        public bool TimerCRan
        {
            get
            {
                return timerCRan;
            }
            set
            {
                timerCRan = value;
                RaisePropertyChangedEvent("TimerCRan");
            }
        }

        public string TimerC
        {
            get { return tick.ToString(); }
            set
            {
                TimerC = value;
                RaisePropertyChangedEvent("TimerC");
            }
        }
    }
}
