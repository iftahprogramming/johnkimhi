﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilitiesLib.DAL;

namespace JohnKimhi.BL.GlobalManagers
{

    public static class CommandsManager
    {
        private static List<byte> reply_OK = new List<byte>() { 0x21 };

        enum CommandsFirstWord
        {
            ProgFirstSens = 0xc1,
            ProgSecondSens = 0xc2,
            ProgPollingWindow = 0xda,
            ProgBootTime = 0xbd,
        };

        private static List<byte> enterPC_Mode = new List<byte>() { 0xe1, 0xfc, 0x29, 0x93, 0xb5, 0x12 };
        private static List<byte> exitPC_Mode = new List<byte>() { 0xec, 0x9b, 0x74, 0x38, 0xd0, 0x3c };

        /// <summary>
        /// Return "Command" that enter PC Mode
        /// </summary>
        public static CommCommand EnterPCMode()
        {
            CommCommand command = new CommCommand("EnterPCMode", reply_OK.Count, 3000, 0, AddCRCToCommand(enterPC_Mode), reply_OK);
            return command;
        }

        /// <summary>
        /// Return "Command" that exit PC Mode
        /// </summary>
        public static CommCommand ExitPCMode()
        {
            CommCommand command = new CommCommand("ExitPCMode", reply_OK.Count, 3000, 0, AddCRCToCommand(exitPC_Mode), reply_OK);
            return command;
        }

        private static List<byte> AddCRCToCommand(List<byte> commandList)
        {
            byte crc = 0x00;
            for (int i = 0; i < commandList.Count; i++)
                crc ^= commandList[i];
            commandList.Add(crc);
            return commandList;
        }

        public static CommCommand PollingWindow(Int16 pollingTime)
        {
            List<byte> pollingWindowList = new List<byte>();

            byte[] intBytes = BitConverter.GetBytes(pollingTime);
            pollingWindowList.Add((byte)CommandsFirstWord.ProgPollingWindow);
            pollingWindowList.AddRange(intBytes);
            pollingWindowList.AddRange(new byte[] { 0x77, 0x88, 0x99 });

            CommCommand command = new CommCommand("PollingWindowProg", reply_OK.Count, 3000, 0, AddCRCToCommand(pollingWindowList), reply_OK);
            return command;
        }

        public static CommCommand BootDurationProg(Int16 bootTime)
        {
            List<byte> bootTimeList = new List<byte>();

            byte[] intBytes = BitConverter.GetBytes(bootTime);
            bootTimeList.Add((byte)CommandsFirstWord.ProgBootTime);
            bootTimeList.AddRange(intBytes);
            bootTimeList.AddRange(new byte[] { 0x77, 0x88, 0x99 });

            CommCommand command = new CommCommand("BootDurationProg", reply_OK.Count, 3000, 0, AddCRCToCommand(bootTimeList), reply_OK);
            return command;
        }

        public static CommCommand Sens1Write(byte sensID, Int32 sensThresh)
        {
            List<byte> Sens1Prog = new List<byte>();

            byte[] intBytes = BitConverter.GetBytes(sensThresh);
            Sens1Prog.Add((byte)CommandsFirstWord.ProgFirstSens);
            Sens1Prog.Add(sensID);
            Sens1Prog.AddRange(intBytes);

            CommCommand command = new CommCommand("Sens1Prog", reply_OK.Count, 3000, 0, AddCRCToCommand(Sens1Prog), reply_OK);
            return command;
        }

        public static CommCommand Sens2Write(byte sensID, Int32 sensThresh)
        {
            List<byte> Sens2Prog = new List<byte>();

            byte[] intBytes = BitConverter.GetBytes(sensThresh);
            Sens2Prog.Add((byte)CommandsFirstWord.ProgSecondSens);
            Sens2Prog.Add(sensID);
            Sens2Prog.AddRange(intBytes);

            CommCommand command = new CommCommand("Sens1Prog", reply_OK.Count, 3000, 0, AddCRCToCommand(Sens2Prog), reply_OK);
            return command;
        }

    }
}
