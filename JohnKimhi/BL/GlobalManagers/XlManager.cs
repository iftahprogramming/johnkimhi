﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JohnKimhi.BL.GlobalManagers
{
    class XlManager
    {
        private DAL.ExcelExporter exporter;

        public void Export(List<DetectionManager> detections)
        {
            for (int i = 0; i < detections.Count; i++)
            {
                try
                {
                    detections[i].ActualNumberOfPassbys = 0;
                }
                catch { }
            }

            #region Props
            double Avg = 0;
            double Avg2 = 0;
            int sens1NoiseFloor = 0;
            int sens2NoiseFloor = 0;
            double Threshould1 = 0;
            double Threshould2 = 0;
            #endregion

            #region Excel Headers
            exporter = new DAL.ExcelExporter("Results");
            exporter.SetCellContent("טבלת גילויים", 1, 3);

            exporter.SetCellContent("מרחקים", 2, 1);
            exporter.SetCellContent("מעברים", 3, 1);

            exporter.SetCellContent("AVG", Global.Consts.numberOfPassBys + 5, 2);
            exporter.SetCellContent("NoiseFloor", Global.Consts.numberOfPassBys + 6, 2);
            exporter.SetCellContent("Threshould", Global.Consts.numberOfPassBys + 7, 2);
            #endregion

            #region Sub Headers
            for (int i = 0; i < detections.Count; i++)
            {
                try
                {
                    exporter.SetCellContent(detections[i].Distance.ToString(), 2, 2 * (i + 1) + 1);
                    exporter.SetCellContent(Logic.Instance.UIStateManager.Sensor1Name, 3, 2 * (i + 1) + 1);
                    exporter.SetCellContent(Logic.Instance.UIStateManager.Sensor2Name, 3, 2 * (i + 1) + 2);
                }
                catch { }

                #region NoiseFloor
                try
                {
                    sens1NoiseFloor = Math.Abs(int.Parse(Logic.Instance.UIStateManager.Sensor1Threshold) - int.Parse(Logic.Instance.UIStateManager.Sensor1ActualThreshold));
                    sens2NoiseFloor = Math.Abs(int.Parse(Logic.Instance.UIStateManager.Sensor2Threshold) - int.Parse(Logic.Instance.UIStateManager.Sensor2ActualThreshold));
                    exporter.SetCellContent(sens1NoiseFloor.ToString(), Global.Consts.numberOfPassBys + 6, 2 * (i + 1) + 1);
                    exporter.SetCellContent(sens2NoiseFloor.ToString(), Global.Consts.numberOfPassBys + 6, 2 * (i + 1) + 2);
                }
                catch { }

                #endregion
            }
            #endregion

            #region Values
            for (int i = 0; i < 10; i++)
            {
                exporter.SetCellContent("מעבר " + (i + 1).ToString(), 4 + i, 1);
                for (int j = 0; j < detections.Count; j++)
                {
                    try
                    {
                        exporter.SetCellContent(detections[j].Detections[i].FirstNumberOfDetection, 4 + i, 2 * (j + 1) + 1);
                        exporter.SetCellContent(detections[j].Detections[i].SecondNumberOfDetection, 4 + i, 2 * (j + 1) + 2);

                        if (!detections[j].Detections[i].FirstNumberOfDetection.Equals("") && !detections[j].Detections[i].FirstNumberOfDetection.Equals(null))
                            detections[j].ActualNumberOfPassbys++;
                    }
                    catch { }
                }
            }
            #endregion

            #region AVG & Threshould
            for (int i = 0; i < detections.Count; i++)
            {
                Avg = 0;
                Avg2 = 0;
                Threshould1 = 0;
                Threshould2 = 0;

                for (int j = 0; j < detections[i].ActualNumberOfPassbys; j++)
                {
                    try
                    {
                        Avg += int.Parse(detections[i].Detections[j].FirstNumberOfDetection);
                        Avg2 += int.Parse(detections[i].Detections[j].SecondNumberOfDetection);
                    }
                    catch { }
                }

                #region AVG
                try
                {
                    if (detections[i].ActualNumberOfPassbys > 0)
                    {
                        Avg = Avg / detections[i].ActualNumberOfPassbys;
                        Avg2 = Avg2 / detections[i].ActualNumberOfPassbys;
                        exporter.SetCellContent(Avg.ToString(), Global.Consts.numberOfPassBys + 5, 2 * (i + 1) + 1); //AVG
                        exporter.SetCellContent(Avg2.ToString(), Global.Consts.numberOfPassBys + 5, 2 * (i + 1) + 2); //AVG2 
                    }

                }
                catch { }
                #endregion
                
                #region Threshould
                try
                {
                    if (detections[i].ActualNumberOfPassbys > 0)
                    {
                        Threshould1 = (double)(Global.Consts.PercentCalcSensor1 * (Math.Abs((double)Avg - sens1NoiseFloor)));
                        Threshould2 = (double)(Global.Consts.PercentCalcSensor2 * (Math.Abs((double)Avg2 - sens2NoiseFloor)));
                        exporter.SetCellContent(Threshould1.ToString(), Global.Consts.numberOfPassBys + 7, 2 * (i + 1) + 1);
                        exporter.SetCellContent(Threshould2.ToString(), Global.Consts.numberOfPassBys + 7, 2 * (i + 1) + 2);
                    }

                }
                catch { }
                #endregion
            }
            #endregion

            #region Save As Dialog
            SaveFileDialog File = new SaveFileDialog();
            if (File.ShowDialog() == DialogResult.OK)
            {
                exporter.Save(File.FileName);
            }
            #endregion
        }

    }
}
