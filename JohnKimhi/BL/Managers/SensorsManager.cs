﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JohnKimhi.BL.Managers
{
    // Contains adding and removing functions.
    // Loading sensors's list
    // Contains serializing function - from List<Sensor> to json and from Json to List<sensor>
    class SensorsManager
    {
        public void AddSensor()
        {

        }

        public ListSensors LoadSensors(string json)
        {
            try
            {
                return JsonConvert.DeserializeObject<ListSensors>(json);
            }
            catch
            {
               return new ListSensors();
            }
        }
    }
}
