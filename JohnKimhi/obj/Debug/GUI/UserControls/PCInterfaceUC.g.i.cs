﻿#pragma checksum "..\..\..\..\GUI\UserControls\PCInterfaceUC.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "FCAC0E0A61776292CA6F084E853DC399"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using JohnKimhi.GUI.UserControls;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace JohnKimhi.GUI.UserControls {
    
    
    /// <summary>
    /// PCInterfaceUC
    /// </summary>
    public partial class PCInterfaceUC : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 31 "..\..\..\..\GUI\UserControls\PCInterfaceUC.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox SamplesWindow;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\..\..\GUI\UserControls\PCInterfaceUC.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ReadButton;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\..\..\GUI\UserControls\PCInterfaceUC.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ProgramButton;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/JohnKimhi;component/gui/usercontrols/pcinterfaceuc.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\GUI\UserControls\PCInterfaceUC.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.SamplesWindow = ((System.Windows.Controls.TextBox)(target));
            
            #line 31 "..\..\..\..\GUI\UserControls\PCInterfaceUC.xaml"
            this.SamplesWindow.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.SamplesWindow_PreviewTextInput);
            
            #line default
            #line hidden
            return;
            case 2:
            this.ReadButton = ((System.Windows.Controls.Button)(target));
            
            #line 32 "..\..\..\..\GUI\UserControls\PCInterfaceUC.xaml"
            this.ReadButton.Click += new System.Windows.RoutedEventHandler(this.ReadButton_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.ProgramButton = ((System.Windows.Controls.Button)(target));
            
            #line 33 "..\..\..\..\GUI\UserControls\PCInterfaceUC.xaml"
            this.ProgramButton.Click += new System.Windows.RoutedEventHandler(this.ProgramButton_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

