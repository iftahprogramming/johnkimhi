﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace JohnKimhi.GUI.Windows
{
    /// <summary>
    /// Interaction logic for RunningWindow.xaml
    /// </summary>
    public partial class RunningWindow : Window
    {
        public RunningWindow()
        {
            InitializeComponent();
            this.DataContext = BL.Logic.Instance.UIStateManager;

            if (BL.Logic.Instance.UIStateManager.timerC.IsEnabled)
            {
                BL.Logic.Instance.Rs232Connection.ReceivingLength = 8;
                BL.Logic.loaded = true;
            }
            else if (BL.Logic.Instance.UIStateManager.TimerCRan)
            {
                BL.Logic.Instance.Rs232Connection.ReceivingLength = 9;
            }
            else
            {
                if (!BL.Logic.loaded)
                {
                    BL.Logic.Instance.Rs232Connection.ReceivingLength = 2;
                    BL.Logic.loaded = true;
                }
                else
                    BL.Logic.Instance.Rs232Connection.ReceivingLength = 9;
            }


        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BL.Logic.Instance.Rs232Connection.ClearReceivingList();
            if (RunMode.IsSelected)
            {
                BL.Logic.Instance.UIStateManager.RunOrRecordMode = true;
                this.Height = 500;
                this.Width = 650;
            }
            else
            {
                BL.Logic.Instance.UIStateManager.RunOrRecordMode = false;
                this.Height = 425;
                this.Width = 425;
            }
        }

        private void BackHomeButton_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                this.Close();
            }
            catch { }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (BL.Logic.Instance.UIStateManager.TestingThreshould)
            {
                MessageBoxResult result = MessageBox.Show("האם אתה בטוח שאתה רוצה לצאת? כל התהליכים יתאפסו", "אזהרה", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes)
                {
                    BackHomeButton_MouseDown(null, null);
                }
                else
                {
                    e.Cancel = true;
                    return;
                }
            }
            BL.Logic.Instance.Rs232Connection.ReceivingLength = 2;
            BL.Logic.Instance.UIStateManager.RecClicked = false;
            BL.Logic.Instance.UIStateManager.timerC.Stop();
            BL.Logic.Instance.UIStateManager.TestingThreshould = false;
            BL.Logic.Instance.UIStateManager.Tick = Global.Consts.Ticks;
        }
    }
}
