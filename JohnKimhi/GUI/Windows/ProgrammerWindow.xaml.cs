﻿using JohnKimhi.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace JohnKimhi.GUI.Windows
{
    /// <summary>
    /// Interaction logic for ProgrammerWindow.xaml
    /// </summary>
    public partial class ProgrammerWindow : Window
    {
        public ProgrammerWindow()
        {
            InitializeComponent();
            this.DataContext = Logic.Instance.UIStateManager;
        }


        private void Grid_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (passwordGrid.Visibility == Visibility.Visible)
            {
                this.Width = 300;
                this.Height = 300;
            }
            else
            {
                this.Width = 600;
                this.Height = 600;
            }
        }

        private void BackHomeButton_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                BL.Logic.Instance.Rs232Connection.SendCommandWithDelay(BL.GlobalManagers.CommandsManager.ExitPCMode());
                Logic.Instance.Rs232Connection.ReceivingLength = 2;
                this.Close();
            }
            catch { }
        }
    }
}
