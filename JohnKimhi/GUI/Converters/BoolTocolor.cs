﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace JohnKimhi.GUI.Converters
{
    public class BoolToColor:IValueConverter
    {
        public Brush greenColor = new SolidColorBrush(Colors.Green);
        public Brush redColor = new SolidColorBrush(Colors.Red);

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? greenColor : redColor;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
