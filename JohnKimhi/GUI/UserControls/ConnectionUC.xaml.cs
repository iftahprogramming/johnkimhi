﻿using JohnKimhi.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JohnKimhi.Global;

namespace JohnKimhi.GUI.UserControls
{
    /// <summary>
    /// Interaction logic for ConnectionUC.xaml
    /// </summary>
    public partial class ConnectionUC : UserControl
    {
        public ConnectionUC()
        {
            InitializeComponent();
            this.DataContext = Logic.Instance.UIStateManager;
            BaudRateCB.ItemsSource = Consts.BaudRateList;
        }

        private void ConnectButton_Click(object sender, RoutedEventArgs e)
        {
            if (PortsComboBox.Text != string.Empty && BaudRateCB.Text != string.Empty)
                Logic.Instance.Connect(PortsComboBox.Text, int.Parse(BaudRateCB.Text));
            else
                Logic.Instance.UIStateManager.IsConnected = false;
        }
    }
}
