﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JohnKimhi.Global;

namespace JohnKimhi.GUI.UserControls.ProgrammerUI
{
    /// <summary>
    /// Interaction logic for PCInterfaceUC.xaml
    /// </summary>
    public partial class PCInterfaceUC : UserControl
    {
        public PCInterfaceUC()
        {
            InitializeComponent();
        }

        private void ReadButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ProgramButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BL.Logic.Instance.UIStateManager.PollingWindow = int.Parse(SamplesWindow.Text);
                BL.Logic.Instance.Rs232Connection.SendCommandWithDelay(BL.GlobalManagers.CommandsManager.PollingWindow(Global.GlobalMethods.CalculatePollingWindow(int.Parse(SamplesWindow.Text))));
            }
            catch { }
        }

        private void SamplesWindow_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.OnlyNumInput();
        }

        private void ProgramButton1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BL.Logic.Instance.Rs232Connection.SendCommandWithDelay(BL.GlobalManagers.CommandsManager.BootDurationProg(Global.GlobalMethods.CalculateBootWindow(int.Parse(BootDurationWindowTBox.Text))));
            }
            catch { }
            }

        private void ConnectButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BL.Logic.Instance.Rs232Connection.SendCommandWithDelay(BL.GlobalManagers.CommandsManager.EnterPCMode());
                //BL.Logic.Instance.UIStateManager.Sensor1ActualThreshold = "";
                //BL.Logic.Instance.UIStateManager.Sensor2ActualThreshold = "";
                ConnectButton.IsEnabled = false;
                DisconnectButton.IsEnabled = true;
            }
            catch { }
        }

        private void DisconnectButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BL.Logic.Instance.Rs232Connection.SendCommandWithDelay(BL.GlobalManagers.CommandsManager.ExitPCMode());
                BL.Logic.Instance.Rs232Connection.ReceivingLength = 2;
                DisconnectButton.IsEnabled = false;
                ConnectButton.IsEnabled = true;
            }
            catch
            {

            }
        }
    }
}
