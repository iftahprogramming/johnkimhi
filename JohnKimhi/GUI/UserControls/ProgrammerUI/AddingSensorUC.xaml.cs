﻿using JohnKimhi.BL;
using JohnKimhi.DAL.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using JohnKimhi.Global;

namespace JohnKimhi.GUI.UserControls.ProgrammerUI
{
    /// <summary>
    /// Interaction logic for AddingSensorUC.xaml
    /// </summary>
    public partial class AddingSensorUC : UserControl
    {
        public string imageSourceFilePath;

        public AddingSensorUC()
        {
            InitializeComponent();
        }

        private void AddingSensorButton_Click(object sender, RoutedEventArgs e)
        {
            /*if (SensorImage.Source == null)
            {
                if (MessageBox.Show("אתה בטוח שאתה רוצה להוסיף חיישן ללא תמונה?", "", MessageBoxButton.YesNo) == MessageBoxResult.No)
                    return;
            }*/
            if (SensorName.Text == string.Empty)
            {
                MessageBox.Show("יש להזין שם חיישן");
                return;
            }

            Logic.Instance.AddSensor(SensorName.Text, SensorDescription.Text, imageSourceFilePath);
        }

        private void BrowseButton_Click(object sender, RoutedEventArgs e)
        {
            //imageSourceFilePath = FilesClass.OpenFile();
            //if (imageSourceFilePath != string.Empty)

                //SensorImage.Source = imageSourceFilePath.FilePathToImage();

            //string[] imageName = imageSourceFilePath.Split(new char[] { '\\' });

        }
    }
}
