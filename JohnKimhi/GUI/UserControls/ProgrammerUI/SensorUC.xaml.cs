﻿using JohnKimhi.BL;
using JohnKimhi.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UtilitiesLib.DAL;

namespace JohnKimhi.GUI.UserControls.ProgrammerUI
{
    /// <summary>
    /// Interaction logic for SensorUC.xaml
    /// </summary>
    public partial class SensorUC : UserControl
    {
        public int SensorIndex
        {
            get { return (int)GetValue(SensorIndexProperty); }
            set { SetValue(SensorIndexProperty, value); }
        }

        public static readonly DependencyProperty SensorIndexProperty =
            DependencyProperty.Register("SensorIndex", typeof(int), typeof(SensorUC), new UIPropertyMetadata(0));

        public SensorUC()
        {
            InitializeComponent();
            this.DataContext = Logic.Instance.UIStateManager;
        }

        private void TreasholdTB_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.OnlyNumInput();
        }

        private void SensorsNameCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = sender as ComboBox;
            Sensor chosenSensor = Logic.Instance.Sensors.SensorsList.Where(s => s.Name == box.SelectedItem).First();
            DescriptionTB.Text = chosenSensor.Description;
            SensorImage.Source = (chosenSensor.ImageName == "null" || chosenSensor.ImageName == "" || chosenSensor.ImageName == null) ? null : chosenSensor.ImageName.FilePathToImage();
        }

        private void SensProgButton_Click(object sender, RoutedEventArgs e)
        {
            if (TreasholdTB.Text == string.Empty || SensorsNameCB.Text == string.Empty)
                return;
            try
            {
                byte sensID = 0;
                foreach (var sensor in BL.Logic.Instance.Sensors.SensorsList)
                {
                    if (sensor.Name == SensorsNameCB.Text)
                    {
                        sensID = (byte)sensor.ID;
                        break;
                    }
                }
                CommCommand Command = null;

                if (SensorIndex.Equals(1))
                    Command = BL.GlobalManagers.CommandsManager.Sens1Write(sensID, int.Parse(TreasholdTB.Text));
                else
                    Command = BL.GlobalManagers.CommandsManager.Sens2Write(sensID, int.Parse(TreasholdTB.Text));

                BL.Logic.Instance.Rs232Connection.SendCommandWithDelay(Command);
                BL.Logic.Instance.UpdateSensor(SensorsNameCB.Text, int.Parse(TreasholdTB.Text));
            }
            catch (Exception ex) { }
        }
    }
}
