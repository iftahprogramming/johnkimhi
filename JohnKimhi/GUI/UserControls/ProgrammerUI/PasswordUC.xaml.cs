﻿using JohnKimhi.BL;
using JohnKimhi.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace JohnKimhi.GUI.UserControls.ProgrammerUI
{
    /// <summary>
    /// Interaction logic for PasswordUC.xaml
    /// </summary>
    public partial class PasswordUC : UserControl
    {
        public PasswordUC()
        {
            InitializeComponent();
            Logic.Instance.UIStateManager.IsProgModePassCorrect = false;
        }

        private void ProgrammerMode_Click(object sender, RoutedEventArgs e)
        {
            if (PasswordTB.Password == Consts.programmerMode_Password)
                Logic.Instance.UIStateManager.IsProgModePassCorrect = true;
        }

        private void PasswordTB_Loaded(object sender, RoutedEventArgs e)
        {
            PasswordTB.Focus();
        }

        private void PasswordTB_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                ProgrammerMode_Click(null, null);
        }

   

       
    }
}
