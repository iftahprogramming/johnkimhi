﻿using JohnKimhi.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace JohnKimhi.GUI.UserControls.UnitUI
{
    /// <summary>
    /// Interaction logic for OneRecordUC.xaml
    /// </summary>
    public partial class OneRecordUC : UserControl
    {
        public OneRecordUC()
        {
            InitializeComponent();
            this.DataContext = BL.Logic.Instance.detectionManagers;
            OutOfTBlock.DataContext = Global.Consts.numberOfPassBys;
            //this.DataContext = BL.Logic.Instance.detectionManagers;

        }
        public int DetectionIndex
        {
            get { return (int)GetValue(DetectionIndexProperty); }
            set { SetValue(DetectionIndexProperty, value); }
        }

        public static readonly DependencyProperty DetectionIndexProperty =
            DependencyProperty.Register("DetectionIndex", typeof(int), typeof(OneRecordUC), new UIPropertyMetadata(0));

        private void RecordButton_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                Logic.Instance.Rs232Connection.ClearReceivingList();
                BL.Logic.Instance.UIStateManager.RecClicked = true;
                BL.Logic.Instance.detectionManagers[DetectionIndex].Distance = int.Parse(DistanceTBox.Text);
                BL.Logic.Instance.DetectionManagersIndex = DetectionIndex;
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("מלא את כל השדות בשביל להקליט");
            }
        }


        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            BL.Logic.Instance.UIStateManager.RecClicked = false;
            BL.Logic.Instance.detectionManagers[DetectionIndex].Detections.Clear();
            DistanceTBox.Text = "";
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Binding b = new Binding();
            b.Source = BL.Logic.Instance.detectionManagers;
            b.Path = new PropertyPath(".[" + DetectionIndex + "].Detections.Count");
            b.UpdateSourceTrigger = UpdateSourceTrigger.Default;
            BufferNum.SetBinding(TextBlock.TextProperty, b);
        }


    }
}
