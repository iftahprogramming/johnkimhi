﻿using JohnKimhi.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace JohnKimhi.GUI.UserControls.UnitUI
{
    /// <summary>
    /// Interaction logic for RecordWindowUC.xaml
    /// </summary>
    public partial class RecordWindowUC : UserControl
    {
        public RecordWindowUC()
        {
            InitializeComponent();
            this.DataContext = BL.Logic.Instance;
            Wait.DataContext = BL.Logic.Instance.UIStateManager;
            Go.DataContext = BL.Logic.Instance.UIStateManager;
            BL.Logic.Instance.Rs232Connection.ReceivingLength = 2;
        }

        private void AddUC_MouseDown(object sender, MouseButtonEventArgs e)
        {
            
        }

        private void XlExporterButton_Click(object sender, RoutedEventArgs e)
        {
            BL.Logic.Instance.UIStateManager.RecClicked = false;
            BL.Logic.Instance.xlManager.Export(Logic.Instance.detectionManagers);

            #region Excel Example
            //List<DetectionManager> detections = new List<DetectionManager>()
            //{
            //    new DetectionManager()
            //    {
            //        Distance = 3,
            //        Detections = new System.Collections.ObjectModel.ObservableCollection<Detection>()
            //        {
            //           new Detection() {FirstNumberOfDetection=301, SecondNumberOfDetection=302},
            //           new Detection() {FirstNumberOfDetection=311, SecondNumberOfDetection=312},
            //           new Detection() {FirstNumberOfDetection=321, SecondNumberOfDetection=322},
            //           new Detection() {FirstNumberOfDetection=331, SecondNumberOfDetection=332},
            //           new Detection() {FirstNumberOfDetection=341, SecondNumberOfDetection=342},
            //           new Detection() {FirstNumberOfDetection=351, SecondNumberOfDetection=352},
            //           new Detection() {FirstNumberOfDetection=361, SecondNumberOfDetection=362},
            //           new Detection() {FirstNumberOfDetection=371, SecondNumberOfDetection=372},
            //           new Detection() {FirstNumberOfDetection=381, SecondNumberOfDetection=382},
            //           new Detection() {FirstNumberOfDetection=391, SecondNumberOfDetection=392}
            //        }
            //    },
            //    new DetectionManager()
            //    {
            //        Distance = 5,
            //        Detections = new System.Collections.ObjectModel.ObservableCollection<Detection>()
            //        {
            //           new Detection() {FirstNumberOfDetection=501, SecondNumberOfDetection=502},
            //           new Detection() {FirstNumberOfDetection=511, SecondNumberOfDetection=512},
            //           new Detection() {FirstNumberOfDetection=521, SecondNumberOfDetection=522},
            //           new Detection() {FirstNumberOfDetection=531, SecondNumberOfDetection=532},
            //           new Detection() {FirstNumberOfDetection=541, SecondNumberOfDetection=542},
            //           new Detection() {FirstNumberOfDetection=551, SecondNumberOfDetection=552},
            //           new Detection() {FirstNumberOfDetection=561, SecondNumberOfDetection=562},
            //           new Detection() {FirstNumberOfDetection=571, SecondNumberOfDetection=572},
            //           new Detection() {FirstNumberOfDetection=581, SecondNumberOfDetection=582},
            //           new Detection() {FirstNumberOfDetection=591, SecondNumberOfDetection=592}
            //        }
            //    },
            //         new DetectionManager()
            //    {
            //        Distance = 7,
            //        Detections = new System.Collections.ObjectModel.ObservableCollection<Detection>()
            //        {
            //           new Detection() {FirstNumberOfDetection=701, SecondNumberOfDetection=702},
            //           new Detection() {FirstNumberOfDetection=711, SecondNumberOfDetection=712},
            //           new Detection() {FirstNumberOfDetection=721, SecondNumberOfDetection=722},
            //           new Detection() {FirstNumberOfDetection=731, SecondNumberOfDetection=732},
            //           new Detection() {FirstNumberOfDetection=741, SecondNumberOfDetection=742},
            //           new Detection() {FirstNumberOfDetection=751, SecondNumberOfDetection=752},
            //           new Detection() {FirstNumberOfDetection=761, SecondNumberOfDetection=762},
            //           new Detection() {FirstNumberOfDetection=771, SecondNumberOfDetection=772},
            //           new Detection() {FirstNumberOfDetection=781, SecondNumberOfDetection=782},
            //           new Detection() {FirstNumberOfDetection=791, SecondNumberOfDetection=792}
            //        }
            //    }
            //};
            //BL.Logic.Instance.xlManager.Export(detections); 
            #endregion
        }
    }
}
