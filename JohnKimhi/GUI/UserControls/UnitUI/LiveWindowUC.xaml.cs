﻿using JohnKimhi.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace JohnKimhi.GUI.UserControls.UnitUI
{
    /// <summary>
    /// Interaction logic for LiveWindowUC.xaml
    /// </summary>
    public partial class LiveWindowUC : UserControl
    {
        public LiveWindowUC()
        {
            InitializeComponent();
            this.DataContext = Logic.Instance.UIStateManager;
        }

        private void ClearAll_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("האם אתה בטוח שאתה רוצה לנקות? חלק מהנתונים יתאפסו", "אזהרה", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                Logic.Instance.UIStateManager.Sens1TrigNum = "";
                Logic.Instance.UIStateManager.Sensor1ActualThreshold = "";
                Logic.Instance.UIStateManager.Sens2TrigNum = "";
                Logic.Instance.UIStateManager.Sensor2ActualThreshold = "";
                Logic.Instance.UIStateManager.TimerCRan = false;
                Logic.loaded = false;
                Logic.Instance.Rs232Connection.ReceivingLength = 2;
                Logic.Instance.ResetParams();
                Logic.Instance.Rs232Connection.ClearReceivingList();
            }

            else
            {
                return;
            }
        }
    }
}
