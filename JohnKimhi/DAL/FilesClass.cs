﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Win32;
using JohnKimhi.Global;
using System.IO;

namespace JohnKimhi.DAL.IO
{
    public static class FilesClass
    {
        private static OpenFileDialog OpenDialog;

        public static string OpenFile(string filter = Consts.AllFilesFilter)
        {
            OpenDialog = new OpenFileDialog();
            OpenDialog.Filter = filter;
            if (OpenDialog.ShowDialog().Value)
                return OpenDialog.FileName;
            else
                return String.Empty;
        }

        public static void Create(string filePath)
        {
            if (!File.Exists(filePath))
                File.Create(filePath);
        }

        public static void CreateDirectory(string directory)
        {
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);
        }

        public static string Read(string directory, string fileName)
        {
            CreateDirectory(directory);
            Create(directory + fileName);

            return File.ReadAllText(directory + fileName);            
        }

        public static void Copy(string file1, string desfile2)
        {

            File.Copy(file1, desfile2);
        }

        public static void Save(string directory, string fileName, string text)
        {
            CreateDirectory(directory);
            Create(directory + fileName);

            File.WriteAllText(directory + fileName, text);
        }
        
    }
}
