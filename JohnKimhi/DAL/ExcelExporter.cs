﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;


namespace JohnKimhi.DAL
{
    class ExcelExporter : IDisposable
    {
        #region DM
        private Excel.Application xlApp;
        private Excel.Workbooks xlWorkbooks;
        private Excel.Workbook xlWorkbook;
        private Excel._Worksheet xlWorksheet;
        #endregion

        #region Props
        public bool XlVisibility
        {
            set
            {
                xlApp.Visible = value;
            }
        }
        
        public XlDirection XlRightToLeft
        {
            set
            {
                xlApp.DefaultSheetDirection = value== XlDirection.xlRTL ? (int)Excel.Constants.xlRTL : (int)Excel.Constants.xlLTR;
            }
        }

        public enum XlDirection
        {
            xlRTL,
            xlLTR
        }
        #endregion

        public ExcelExporter(string worksheetName)
        {
            xlApp = new Excel.Application();
            xlApp.DisplayAlerts = false;
            xlWorkbooks = xlApp.Workbooks;
            xlWorkbook = xlWorkbooks.Add();
            xlWorksheet = (Excel._Worksheet)xlWorkbook.ActiveSheet;
            xlWorksheet.EnableSelection = Microsoft.Office.Interop.Excel.XlEnableSelection.xlNoSelection;
            xlWorksheet.Name = worksheetName;
            
        }
      
        #region Methods
       

        public void SetCellContent(string cellContent, int row, int column, Color? color = null)
        {
            if (xlApp != null)
            {
                xlWorksheet.Cells[row, column] = cellContent;
                if (color != null)
                    xlWorksheet.Range[row, column].Interior.Color = ColorTranslator.ToOle(color.Value);
            }
        }

        public bool Save(string ExcelFilePath)
        {
            if (xlApp != null)
            {
                try
                {

                    xlWorkbook.SaveAs(ExcelFilePath);
                    try
                    {
                        XlVisibility = true;
                    }
                    catch
                    {
                        Close();
                    }
                        return true;
                }
                catch
                {
                    return false;
                }
            }
            return false;
        }

        public void Close()
        {
            if (xlApp != null)
            {
                try
                {
                    xlWorkbook.Close(0);
                    xlApp.Quit();
                    Marshal.ReleaseComObject(xlWorkbook);
                    Marshal.ReleaseComObject(xlWorkbooks);
                    Marshal.ReleaseComObject(xlApp);


                    xlApp = null;
                    xlWorkbooks = null;
                    xlWorkbook = null;
                }
                catch
                {

                }



                GC.Collect();
                GC.WaitForPendingFinalizers();
            }

        }
        #endregion

        #region Destructor
        ~ExcelExporter()
        {
            Close();
        }

        void IDisposable.Dispose()
        {
            Close();
        }
        #endregion


    }
}
