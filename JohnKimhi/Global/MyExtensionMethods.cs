﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace JohnKimhi.Global
{
    public static class MyExtensionMethods
    {
        public static BitmapImage FilePathToImage(this string filePath)
        {
            //return new BitmapImage(new Uri(filePath));
            return null;
        }

        public static TextCompositionEventArgs OnlyNumInput(this TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9.]");
            e.Handled = regex.IsMatch(e.Text);
            return e;
        }

        public static Int32 ConertByteToInt(this List<byte> byteArray, int offset)
        {
            return BitConverter.ToInt32(byteArray.ToArray(),offset);
        }

    }
}
