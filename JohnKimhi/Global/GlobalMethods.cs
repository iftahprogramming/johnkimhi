﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JohnKimhi.Global
{
    public static class GlobalMethods
    {
        const int freq = 2 * (1000000);
        public static Int16 CalculatePollingWindow(int time)
        {
            return (Int16)((time) * freq / (4 * (65536)));
        }
        public static Int16 CalculateBootWindow(int time)
        {
            return (Int16)((time) * freq / (16 * (65536)));
        }
    }
}
