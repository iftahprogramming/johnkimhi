﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilitiesLib.DAL;

namespace JohnKimhi.Global
{
    class Consts
    {
        public const string AllFilesFilter = "All files *.*| *.*";
        public const string programmerMode_Password = "123456";

        public static string SensorsFileDirectory = Environment.CurrentDirectory + @"\SensorsDB";
        public const string FileName = @"\SensorsDB.json";
        public static int FirstSNSensors = 110;

        public static List<string> BaudRateList = new List<string>() { "110", "300", "600", "1200", "2400", "4800", "9600", "14400", "19200", "38400", "56000", "58600", "115200", "128000", "256000" };

        #region Parameters
        public static int numberOfPassBys = 10;
        public static double PercentCalcSensor1 = 0.85; //*100 = %
        public static double PercentCalcSensor2 = 0.3; //*100 = %

        public static int Ticks = 59; //Seconds

        #endregion

        public delegate void GUIUpdate(CommCommand last);

        public enum ReadedIndex
        {
            Sens1Trigger = 0,
            Sens2Trigger = 4,
            LEDs = 8,

        }

        public static List<byte> BufferedPacketRecieved = new List<byte>();
        public static List<byte> TempPacketRecieved = new List<byte>();

        public const byte OK_Replay = 0x21;
        public const byte EndSample_Replay = 0xfc;
        public const byte NotAknowledge_Reply = 0xdd; 

        public const byte OkOrEndMode = 0x0c;
        public const byte PollingWindowMode = 0x1d;
        public const byte ThreshouldCountMode = 0x50;
        public const byte LiveWindowMode = 0xd2;
        public const byte ThreshouldBurnedMode = 0xef;
    }
}
