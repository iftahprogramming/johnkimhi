﻿using JohnKimhi.BL;
using System;
using System.Windows;
using JohnKimhi.GUI.Windows;

namespace JohnKimhi
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        public MainWindow()
        {

            Logic.Instance.Init();
            InitializeComponent();
        }

        private void ProgrammerModeButton_Click(object sender, RoutedEventArgs e)
        {
            if (OpenPort())
            {
                Logic.Instance.UIStateManager.InProgMode = true;
                ProgrammerWindow pw = new ProgrammerWindow();
                pw.Show();
                pw.Closed += SubWindows_Closed;
                this.Visibility = Visibility.Hidden;
            }
 
        }

        void SubWindows_Closed(object sender, EventArgs e)
        {
            Logic.Instance.UIStateManager.InProgMode = false;
            this.Visibility = Visibility.Visible;
        }

        private void RunningModeButton_Click(object sender, RoutedEventArgs e)
        {
            if (OpenPort())
            {
                RunningWindow pw = new RunningWindow();
                pw.Show();
                pw.Closed += SubWindows_Closed;
                this.Visibility = Visibility.Hidden;
            }
        }

        private bool OpenPort()
        {
            if (Logic.Instance.UIStateManager.PortName != null && Logic.Instance.UIStateManager.BaudRate != null)
            {
                Logic.Instance.Connect(Logic.Instance.UIStateManager.PortName, int.Parse(Logic.Instance.UIStateManager.BaudRate));
                return true;
            }
            else
            {
                Logic.Instance.UIStateManager.IsConnected = false;
                return false;
            }
        }
    }
}
